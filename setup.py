#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

setup(
    author="Ralf Klammer",
    author_email="ralf.klammer@tu-dresden.de",
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: Apache Software License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    description="IAM4NFDI social auth backends adapted from python-social-auth",
    install_requires=["python-social-auth"],
    license="Apache Software License 2.0",
    include_package_data=True,
    keywords="nfdi, social_auth",
    name="nfdi_social_auth",
    packages=find_packages(include=["nfdi_social_auth", "nfdi_social_auth.*"]),
    test_suite="tests",
    url="https://git.rwth-aachen.de/nfdi4earth/architecture/nfdi_social_auth.git",
    version="0.2.3",
    zip_safe=False,
)
