"""
AcademicID IDM backend, docs at:
    https://keycloak.sso.gwdg.de/auth/realms/academiccloud/.well-known/openid-configuration
"""

from social_core.backends.oauth import BaseOAuth2


class BaseAcademicIDAuth:
    def get_user_id(self, details, response):
        """Use email as unique id"""
        if self.setting("USE_UNIQUE_USER_ID", False):
            if "sub" in response:
                return response["sub"]
            else:
                return response["id"]
        else:
            return details["email"]

    def get_user_details(self, response):
        """Return user details from AcademicID API account"""
        email = response.get("email", "")

        name, given_name, family_name = (
            response.get("name", ""),
            response.get("given_name", ""),
            response.get("family_name", ""),
        )

        fullname, first_name, last_name = self.get_user_names(
            name, given_name, family_name
        )
        return {
            "username": email.split("@", 1)[0],
            "email": email,
            "fullname": fullname,
            "first_name": first_name,
            "last_name": last_name,
        }


class BaseAcademicIDOAuth2API(BaseAcademicIDAuth):
    def user_data(self, access_token, *args, **kwargs):
        """Return user data from AcademicID API"""
        return self.get_json(
            "https://keycloak.sso.gwdg.de/auth/realms/academiccloud/protocol/openid-connect/userinfo",
            headers={
                "Authorization": "Bearer %s" % access_token,
            },
        )

    def revoke_token_params(self, token, uid):
        return {"token": token}

    def revoke_token_headers(self, token, uid):
        return {"Content-type": "application/json"}


class AcademicIDOAuth2(BaseAcademicIDOAuth2API, BaseOAuth2):
    """AcademicID OAuth2 authentication backend"""

    name = "academicid-oauth2"
    REDIRECT_STATE = False
    AUTHORIZATION_URL = "https://keycloak.sso.gwdg.de/auth/realms/academiccloud/protocol/openid-connect/auth"
    ACCESS_TOKEN_URL = "https://keycloak.sso.gwdg.de/auth/realms/academiccloud/protocol/openid-connect/token"
    ACCESS_TOKEN_METHOD = "POST"
    REVOKE_TOKEN_URL = "https://keycloak.sso.gwdg.de/auth/realms/academiccloud/protocol/openid-connect/revoke"
    REVOKE_TOKEN_METHOD = "GET"
    # The order of the default scope is important
    DEFAULT_SCOPE = ["email"]
    EXTRA_DATA = [
        ("refresh_token", "refresh_token", True),
        ("expires_in", "expires"),
        ("token_type", "token_type", True),
    ]
