"""
RegApp IDM backend, docs at:
    https://regapp.nfdi-aai.de/oidc/realms/nfdi/.well-known/openid-configuration
"""

from social_core.backends.oauth import BaseOAuth2


class BaseRegAppAuth:
    def get_user_id(self, details, response):
        """Use email as unique id"""
        if self.setting("USE_UNIQUE_USER_ID", False):
            if "sub" in response:
                return response["sub"]
            else:
                return response["id"]
        else:
            return details["email"]

    def get_user_details(self, response):
        """Return user details from RegApp API account"""
        email = response.get("email", "")

        name, given_name, family_name = (
            response.get("name", ""),
            response.get("given_name", ""),
            response.get("family_name", ""),
        )

        fullname, first_name, last_name = self.get_user_names(
            name, given_name, family_name
        )
        return {
            "username": email.split("@", 1)[0],
            "email": email,
            "fullname": fullname,
            "first_name": first_name,
            "last_name": last_name,
        }


class BaseRegAppOAuth2API(BaseRegAppAuth):
    def user_data(self, access_token, *args, **kwargs):
        """Return user data from RegApp API"""
        return self.get_json(
            "https://regapp.nfdi-aai.de/oidc/realms/nfdi/protocol/openid-connect/userinfo",
            headers={
                "Authorization": "Bearer %s" % access_token,
            },
        )

    def revoke_token_params(self, token, uid):
        return {"token": token}

    def revoke_token_headers(self, token, uid):
        return {"Content-type": "application/json"}


class RegAppOAuth2(BaseRegAppOAuth2API, BaseOAuth2):
    """RegApp OAuth2 authentication backend"""

    name = "regapp-oauth2"
    REDIRECT_STATE = False
    AUTHORIZATION_URL = (
        "https://regapp.nfdi-aai.de/oidc/realms/nfdi/protocol/openid-connect/auth"
    )
    ACCESS_TOKEN_URL = (
        "https://regapp.nfdi-aai.de/oidc/realms/nfdi/protocol/openid-connect/token"
    )
    ACCESS_TOKEN_METHOD = "POST"
    REVOKE_TOKEN_URL = ""
    REVOKE_TOKEN_METHOD = "GET"
    # The order of the default scope is important
    DEFAULT_SCOPE = ["email"]
    EXTRA_DATA = [
        ("refresh_token", "refresh_token", True),
        ("expires_in", "expires"),
        ("token_type", "token_type", True),
    ]
